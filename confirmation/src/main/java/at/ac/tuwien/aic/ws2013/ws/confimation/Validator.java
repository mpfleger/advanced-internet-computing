package at.ac.tuwien.aic.ws2013.ws.confimation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.ws.Endpoint;

import net.miginfocom.swing.MigLayout;

public class Validator implements ActionListener {

	private JFrame jframe;
	private JButton buttonOK;
	private JButton buttonOther;
	private JPanel pdfPanel;
	private byte[] base64;
	private Callback callback;

	public Validator() {
		jframe = new JFrame("Validator");

		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		jframe.getContentPane().add(getMainPanel());
		jframe.setMinimumSize(new Dimension(300, 300));
		jframe.setExtendedState(jframe.getExtendedState()
				| JFrame.MAXIMIZED_BOTH);
		jframe.pack();
		jframe.setVisible(true);
	}

	private JPanel getMainPanel() {
		JLabel label = new JLabel("Validate PDF documents");
		label.setFont(label.getFont().deriveFont(30.0f));

		buttonOK = new JButton("PDF is valid");
		buttonOK.setName("ok");
		buttonOK.setFont(label.getFont().deriveFont(15.0f));
		buttonOK.setBackground(Color.GREEN);
		buttonOK.setMinimumSize(new Dimension(215, 40));
		buttonOK.addActionListener(this);

		buttonOther = new JButton("Invalid / Choose other PDF");
		buttonOther.setName("other");
		buttonOther.setFont(label.getFont().deriveFont(15.0f));
		buttonOther.setBackground(Color.orange);
		buttonOther.setMinimumSize(new Dimension(215, 40));
		buttonOther.addActionListener(this);

		pdfPanel = new JPanel(new MigLayout("insets 0"));

		JPanel panelMain = new JPanel(new MigLayout("insets 15"));

		panelMain.add(label, "span, wrap");
		panelMain.add(buttonOK, "gapright 10");
		panelMain.add(buttonOther, "wrap");
		panelMain.add(pdfPanel, "span, width 100%");

		enableButtons(false);

		return panelMain;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JButton button = (JButton) arg0.getSource();

		if (button.getName().equals("ok")) {
			enableButtons(false);
			callback.sendDocument(base64);
		} else {

			JFileChooser fc = new JFileChooser("C:\\");
			fc.setFileFilter(new FileNameExtensionFilter("PDF (*.pdf)", "pdf"));
			int result = fc.showOpenDialog(jframe);

			if (result == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();

				FileInputStream inputStream;
				try {
					inputStream = new FileInputStream(file);

					byte[] data = new byte[(int) file.length()];
					inputStream.read(data);
					inputStream.close();

					enableButtons(false);
					callback.sendDocument(data);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		this.jframe.dispose();
	}

	private void enableButtons(boolean value) {
		buttonOK.setEnabled(value);
		buttonOther.setEnabled(value);

		if (value == false) {
			pdfPanel.removeAll();
			JLabel label = new JLabel("Waiting for next request...");
			label.setFont(label.getFont().deriveFont(20.0f));
			pdfPanel.add(label, "gapTop 20");
		}

		jframe.repaint();
		jframe.validate();
	}

	interface Callback {
		public void sendDocument(byte[] base64);
	}

	public void loadDocument(byte[] base64, Callback callback) {
		this.base64 = base64;
		this.callback = callback;

		UUID id = UUID.randomUUID();

		FileOutputStream output;
		try {
			String file = "./" + id.toString() + ".pdf";
			output = new FileOutputStream(file);
			output.write(base64);
			output.close();

			Viewer viewer = new Viewer();
			viewer.open(file);

			pdfPanel.removeAll();
			pdfPanel.add(viewer, "width 100%");

			enableButtons(true);

			jframe.repaint();
			jframe.validate();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		ConfirmationPT ws = new ConfirmationServiceImpl();
		Endpoint endpoint = Endpoint.publish(
				"http://127.0.0.1:8083/ConfirmationService", ws);

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter exit to stop webservice");

		while (scanner.hasNextLine()) {
			if (scanner.nextLine().equals("exit")) {
				endpoint.stop();
				break;
			}
		}

		scanner.close();
	}
}
