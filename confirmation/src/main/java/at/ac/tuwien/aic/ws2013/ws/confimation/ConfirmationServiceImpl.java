package at.ac.tuwien.aic.ws2013.ws.confimation;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService(name = "ConfirmationPT", targetNamespace = "at.ac.tuwien.aic.ws2013.ws.confirmation")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public class ConfirmationServiceImpl implements ConfirmationPT {

	@Override
	@WebMethod
	@Oneway
	public void confirm(
			@WebParam(name = "PDFReport", targetNamespace = "aicTypes", partName = "parameters") final PDFReport parameters) {
		System.out.println(parameters);
		new Validator().loadDocument(parameters.getReport(),
				new Validator.Callback() {
					@Override
					public void sendDocument(byte[] base64) {
						PDFReport report = new PDFReport();
						report.setReport(base64);

						ConfirmationCallbackPT callback = new SentimentAnalysisService()
								.getSentimentAnalysisCallbackPort();
						callback.callback(report);
					}
				});

	}
}
