package at.ac.tuwien.aic.ws2013.ws.client;

import at.ac.tuwien.aic.ws2013.ws.SentimentAnalysis;
import at.ac.tuwien.aic.ws2013.ws.SentimentAnalysisService;
import at.ac.tuwien.aic.ws2013.ws.StringListResponse;

public class Benchmark {

	public static void main(String[] args) {
		SentimentAnalysis service = new SentimentAnalysisService()
				.getSentimentAnalysisPort();
		final int iterations = 100;
		long totalTime = 0;
		StringListResponse list = new StringListResponse();
		list.getValue().add("apple");
		for (int i = 0; i < iterations; i++) {
			long start = System.currentTimeMillis();
			service.process(list);
			long end = System.currentTimeMillis();
			System.out.println(String.format("%s", end - start));
			totalTime += end - start;
		}
		System.out.println(String.format("Total time: %s", totalTime));
		System.out.println(String.format("AVG time: %s ms", (double) totalTime
				/ (double) iterations));
	}
}
