package at.ac.tuwien.aic.ws2013.ws.client;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.jpedal.Display;
import org.jpedal.PdfDecoder;
import org.jpedal.exception.PdfException;

public class Viewer extends JPanel{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JScrollPane scrollPdf = new JScrollPane();

	JPanel insets = new JPanel();

	PdfDecoder pdf = new PdfDecoder();
	int currentPage = 1;
	float scale = 1.0f;

	private float[] scalings = { 0.01f, 0.1f, 0.25f, 0.5f, 0.75f, 1.0f, 1.25f,
			1.5f, 2.0f, 4.0f, 7.5f, 10.0f };

	private char isFit;

	private int insetV = 25;

	private int insetH = 25;

	private int currentScaling;

	JPanel top = new JPanel();

	JToolBar toolBar = new JToolBar("Tool Bar");
	JButton back = new JButton("Back");
	JButton forward = new JButton("Forward");
	JButton zoomIn = new JButton("Zoom in");
	JButton zoomOut = new JButton("Zoom out");
	JButton fitWidth = new JButton("Fit to Width");
	JButton fitHeight = new JButton("Fit to Height");
	JButton fitPage = new JButton("Fit to Page");

	JLabel pageCounter = new JLabel("Page 0 of 0");


	public Viewer() {
		init();	
	}
	
	public void open(String file){
		if (file != null) {
			try {
				pdf.openPdfFile(file);
				decodePage();
			} catch (PdfException ex) {
				ex.printStackTrace();
			}
		} 
	}

	private void init() {
		// pdf.setBackground(Color.RED);
		PdfDecoder.init(true);

		if (SwingUtilities.isEventDispatchThread()) {

			pdf.setDisplayView(Display.SINGLE_PAGE, Display.DISPLAY_CENTERED);

		} else {
			final Runnable doPaintComponent = new Runnable() {

				@Override
				public void run() {
					pdf.setDisplayView(Display.SINGLE_PAGE,
							Display.DISPLAY_CENTERED);
				}
			};
			SwingUtilities.invokeLater(doPaintComponent);
		}

		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (currentPage > 1)
					currentPage--;

				decodePage();
			}
		});

		forward.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (currentPage < pdf.getPageCount())
					currentPage++;

				decodePage();
			}
		});

		currentScaling = 5;

		zoomIn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// zoom in
				System.out.println("Zoom in");

				if (currentScaling < scalings.length - 1) {

					currentScaling = closestIndex(scale, scalings);

					if (scale >= scalings[closestIndex(scale, scalings)]) {

						currentScaling++;

					}

					System.out.println("Current Scale ======= "
							+ currentScaling);

					scale = scalings[currentScaling];

				}

				pdf.setPageParameters(scale, currentPage);

				pdf.updateUI();

				System.out.println("Scaling =========== " + pdf.getScaling());
				isFit = '+';
			}
		});

		zoomOut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// zoom out
				System.out.println("Zoom out");

				if (currentScaling > 0) {

					currentScaling = closestIndex(scale, scalings);

					if (scale <= scalings[closestIndex(scale, scalings)]) {

						currentScaling--;

					}

					System.out.println("Current Scale ======= "
							+ currentScaling);

					scale = scalings[currentScaling];

				}

				pdf.setPageParameters(scale, currentPage);

				pdf.updateUI();

				System.out.println("Scaling =========== " + pdf.getScaling());
				isFit = '-';
			}
		});

		fitWidth.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Fit to height
				System.out.println("Fit to Width");

				fitToX('w');

			}
		});

		fitHeight.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Fit to height
				System.out.println("Fit to Height");

				fitToX('h');

			}
		});

		fitPage.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Fit to height
				System.out.println("Fit to Page");

				fitToX('p');

			}
		});

		this.addComponentListener(new ComponentListener() {

			@Override
			public void componentResized(ComponentEvent e) {
				fitToX(isFit);
			}

			@Override
			public void componentMoved(ComponentEvent e) {
			}

			@Override
			public void componentShown(ComponentEvent e) {
			}

			@Override
			public void componentHidden(ComponentEvent e) {
			}

		});

		toolBar.add(back);
		toolBar.add(forward);
		toolBar.add(zoomIn);
		toolBar.add(zoomOut);
		toolBar.add(fitWidth);
		toolBar.add(fitHeight);
		toolBar.add(fitPage);
		toolBar.setFloatable(false);

		top.setLayout(new GridLayout(1, 1));
		top.add(toolBar);
		((FlowLayout) insets.getLayout()).setVgap(0);
		((FlowLayout) insets.getLayout()).setHgap(0);
		insets.add(pdf);
		if (insetV > 0 || insetH > 0) {

			insets.setBorder(new EmptyBorder(insetH, insetV, insetH, insetV));

		}
		scrollPdf = new JScrollPane(insets);
		scrollPdf.getVerticalScrollBar().setUnitIncrement(32);
		scrollPdf.getHorizontalScrollBar().setUnitIncrement(32);
		this.setLayout(new BorderLayout());
		this.add(top, BorderLayout.NORTH);

		this.add(scrollPdf, BorderLayout.CENTER);
		this.add(pageCounter, BorderLayout.SOUTH);

		this.setVisible(true);
	}

	

	private void fitToX(char fit) {

		if (fit == 'h') {

			float pageH = pdf.getPdfPageData().getCropBoxHeight2D(currentPage);

			scale = ((float) (scrollPdf.getViewportBorderBounds().getHeight()
					- insetH - insetH) / pageH);

			pdf.setPageParameters(scale, currentPage);

			isFit = fit;

		} else if (fit == 'w') {

			float pageW = pdf.getPdfPageData().getCropBoxWidth2D(currentPage);

			scale = ((float) (scrollPdf.getViewportBorderBounds().getWidth()
					- insetV - insetV) / pageW);

			System.out.println("SCALE ==== " + scale);

			pdf.setPageParameters(scale, currentPage);

			isFit = fit;

		} else if (fit == 'p') {

			if (pdf.getPDFWidth() < pdf.getPDFHeight()) {
				fitToX('h');
			} else {
				fitToX('w');
			}

		}

		pdf.updateUI();
	}

	private static int closestIndex(float scale, float[] scalings) {
		float currentMinDiff = Float.MAX_VALUE;
		int closest = 0;

		for (int i = 0; i < scalings.length - 1; i++) {

			final float diff = Math.abs(scalings[i] - scale);

			if (diff < currentMinDiff) {
				currentMinDiff = diff;
				closest = i;
			}

		}
		return closest;
	}

	private void decodePage() {
		try {
			pageCounter.setText("Page " + currentPage + " of "
					+ pdf.getPageCount());

			
			pdf.setPageParameters(scale, currentPage);
			pdf.decodePage(currentPage);

			// wait to ensure decoded
			pdf.waitForDecodingToFinish();

			pdf.invalidate();
			pdf.updateUI();
			pdf.validate();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
