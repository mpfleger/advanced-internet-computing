package at.ac.tuwien.aic.ws2013.ws.client;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;
import at.ac.tuwien.aic.ws2013.ws.PDFReport;
import at.ac.tuwien.aic.ws2013.ws.SentimentAnalysis;
import at.ac.tuwien.aic.ws2013.ws.SentimentAnalysisService;
import at.ac.tuwien.aic.ws2013.ws.StringListResponse;


public class Client implements ChangeListener, ActionListener {
	
	private ArrayList<JTextField> textfieldList;
	private static ArrayList<JPanel> panelList;
	private ArrayList<JLabel> labelList;
	private static JTabbedPane tabs;
	private static JFrame jframe;

	public Client()
	{
		jframe = new JFrame("Client");
		
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		textfieldList = new ArrayList<JTextField>();
		panelList = new ArrayList<JPanel>();
		labelList = new ArrayList<JLabel>();

		tabs = new JTabbedPane();
		tabs.addTab("Request " + (tabs.getTabCount() + 1), getNewMainPanel("Request " + (tabs.getTabCount() + 1)));
		tabs.addTab("+", getNewMainPanel("Request " + (tabs.getTabCount() + 1)));
		tabs.addChangeListener(this);
		
		jframe.getContentPane().add(tabs);
		jframe.setMinimumSize(new Dimension(300, 300));
		jframe.setExtendedState(jframe.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		jframe.pack();
		jframe.setVisible(true);
	}
	
	private JPanel getNewMainPanel(String name)
	{
		JLabel label = new JLabel(name);
		label.setFont(label.getFont().deriveFont (30.0f));
		labelList.add(label);
		
		JLabel termsLabel = new JLabel("Terms:");
		termsLabel.setSize(new Dimension(200, 30));
		termsLabel.setBorder(new EmptyBorder(0, 0, 0, 20));
		
		JTextField termsTextfield = new JTextField();
		termsTextfield.setMinimumSize(new Dimension(400, 30));
		textfieldList.add(termsTextfield);
		
		JButton submitButton = new JButton("Send");
		submitButton.setMaximumSize(new Dimension(300, 30));
		submitButton.setSize(300, 300);
		submitButton.setName(String.valueOf(tabs.getTabCount()));
		submitButton.addActionListener(this);
		
		JPanel panelTerms = new JPanel(new MigLayout("insets 0"));
		panelTerms.add(termsLabel, "");
		panelTerms.add(termsTextfield, "wrap");
		panelTerms.add(submitButton, "span");
		
		JPanel panelContent = new JPanel(new MigLayout("insets 0"));
		panelContent.add(panelTerms);
		panelList.add(panelContent);
		
		JPanel panelMain = new JPanel(new MigLayout("insets 0"));
		panelMain.add(label, "wrap");
		panelMain.add(panelContent, "width 100%");
		panelMain.setBorder(new EmptyBorder(10, 20, 10, 20));
		
		return panelMain;
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {

        int selectedIndex = tabs.getSelectedIndex();
        
        if(selectedIndex == (tabs.getTabCount() - 1))
        {
        	tabs.setTitleAt(tabs.getTabCount() - 1, "Request " + (tabs.getTabCount()));
    		tabs.addTab("+", getNewMainPanel("Request " + (tabs.getTabCount() + 1)));
        }
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JButton button = (JButton) arg0.getSource();
		final int index = Integer.parseInt(button.getName());
		JTextField textfield = textfieldList.get(index);
		
		final String value = textfield.getText();
		textfield.setEnabled(false);
		
		String newLabel = "Request " + (index+1) + ": " + value;
		
		button.setText("Waiting for result...");
		button.setEnabled(false);
		tabs.setBackgroundAt(index, Color.RED);
		tabs.setTitleAt(index, newLabel);
		labelList.get(index).setText(newLabel);

		new Thread(new Runnable() {
			@Override
			public void run() {
				send(index, value);
			}
		}).start();
	}
	
	private void send(int index, String terms)
	{

		SentimentAnalysis service = new SentimentAnalysisService().getSentimentAnalysisPort();
		StringListResponse stringListResponse = new StringListResponse();
		List<String> list = stringListResponse.getValue();
		for (String term : terms.split(";"))
			list.add(term);

		PDFReport report = service.process(stringListResponse);
		response(index, report.getReport());
	}
	
	public static void response(int index, byte[] pdf)
	{
		tabs.setBackgroundAt(index, Color.GREEN);
		UUID id = UUID.randomUUID();
		
		JPanel panel = panelList.get(index);
		panel.removeAll();
		
		FileOutputStream output;
		try {
			String file = "./" + id.toString() + ".pdf";
			output = new FileOutputStream(file);
			output.write(pdf);
			output.close();
			
			Viewer viewer = new Viewer();
			viewer.open(file);
			
			panel.add(viewer, "width 100%");
			jframe.repaint();
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public static void main(String[] args) {
		new Client();
	}
}
