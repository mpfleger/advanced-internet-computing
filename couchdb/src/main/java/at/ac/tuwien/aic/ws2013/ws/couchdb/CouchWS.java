package at.ac.tuwien.aic.ws2013.ws.couchdb;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.google.common.base.CharMatcher;

@WebService(name = "CouchDbWebService", targetNamespace = "http://couchdb.ws.ws2013.aic.tuwien.ac.at/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
public class CouchWS implements CouchDbWebService {

	public CouchWS() {
		CouchManager.getInstance().setDocuments(Data.getTestData());
	}

	@Override
	@WebMethod
	@WebResult(name = "StringListResponse", targetNamespace = "aicTypes", partName = "parameters")
	public StringListResponse getDocuments(
			@WebParam(name = "StringContainer", targetNamespace = "aicTypes", partName = "parameters") StringContainer parameters) {
		List<String> docs = CouchManager.getInstance().getDocuments(
				parameters.name);
		StringListResponse response = new StringListResponse();
		for (String s : docs) {
			String truncated = s.substring(0, Math.min(s.length(), 54));
			if (CharMatcher.ASCII.matchesAllOf(truncated)) {
				System.out.println(truncated);
				response.getValue().add(truncated);
			} else {
				System.out.println("Filtered: " + truncated);
			}
		}

		return response;
	}
}
