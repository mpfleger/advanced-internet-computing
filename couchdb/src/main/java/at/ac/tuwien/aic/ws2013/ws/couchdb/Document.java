package at.ac.tuwien.aic.ws2013.ws.couchdb;

public class Document {
	private String content;

	public Document(String content) {
		this.setContent(content);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
