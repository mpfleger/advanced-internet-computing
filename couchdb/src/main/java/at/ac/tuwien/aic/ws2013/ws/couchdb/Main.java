package at.ac.tuwien.aic.ws2013.ws.couchdb;

import java.util.Scanner;

import javax.xml.ws.Endpoint;

public class Main {

	public static void main(String[] args) {
		Endpoint endpoint = Endpoint.publish(
				"http://127.0.0.1:8081/CouchDbWebService", new CouchWS());

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter exit to stop webservice");

		while (scanner.hasNextLine()) {
			if (scanner.nextLine().equals("exit")) {
				endpoint.stop();
				break;
			}
		}

		scanner.close();
	}
}
