package at.ac.tuwien.aic.ws2013.ws.couchdb;

import java.util.ArrayList;
import java.util.List;

import org.lightcouch.CouchDbClient;
import org.lightcouch.DesignDocument;

public class CouchManager {

	private CouchDbClient dbClient;
	private static CouchManager instance = null;

	private CouchManager() {
		dbClient = new CouchDbClient();
	}

	public static CouchManager getInstance() {
		if (instance == null) {
			instance = new CouchManager();
		}

		return instance;
	}

	public List<String> getDocuments(String term) {
		System.out.println(String.format("Gettings documents for term=%s", term));
		DesignDocument.MapReduce mapReduce = new DesignDocument.MapReduce();
		mapReduce.setMap("" + "function(doc) {" 
				+ "if(doc.content) {"
					+ "if(doc.content.search(/" + term + "/i) != -1) {"
						+ "emit(doc);" 
					+ "}" 
				+ "}" 
			+ "}");

		List<Document> docs = dbClient.view("_temp_view").tempView(mapReduce)
				.includeDocs(true).query(Document.class);
		ArrayList<String> output = new ArrayList<String>();

		for (Document doc : docs) {
			output.add(doc.getContent());
		}

		return output;
	}

	public void setDocuments(List<String> docs) {
		if (dbClient.context().getAllDbs().contains("docs")) {
			dbClient.context().deleteDB("docs", "delete database");
		}
		dbClient.context().createDB("docs");
		for (String doc : docs) {
			dbClient.save(new Document(doc));
		}
	}
}
