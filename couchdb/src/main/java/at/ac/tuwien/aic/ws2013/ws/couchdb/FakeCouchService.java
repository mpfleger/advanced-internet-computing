package at.ac.tuwien.aic.ws2013.ws.couchdb;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import at.ac.tuwien.aic.ws2013.ws.couchdb.CouchDbWebService;
import at.ac.tuwien.aic.ws2013.ws.couchdb.StringContainer;
import at.ac.tuwien.aic.ws2013.ws.couchdb.StringListResponse;

@WebService(name = "CouchDbWebService", targetNamespace = "http://couchdb.ws.ws2013.aic.tuwien.ac.at/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
public class FakeCouchService implements CouchDbWebService {

	private int counter = 1;

	@Override
	@WebMethod
	@WebResult(name = "StringListResponse", targetNamespace = "aicTypes", partName = "parameters")
	public StringListResponse getDocuments(
			@WebParam(name = "StringContainer", targetNamespace = "aicTypes", partName = "parameters") StringContainer parameters) {
		System.out.println(parameters.getName());
		StringListResponse rp = new StringListResponse();
		rp.getValue().add("Couch: " + parameters.getName() + counter++);
		return rp;
	}
}
