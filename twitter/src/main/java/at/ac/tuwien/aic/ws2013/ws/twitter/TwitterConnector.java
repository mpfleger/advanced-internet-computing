package at.ac.tuwien.aic.ws2013.ws.twitter;

import java.util.List;
import twitter4j.TwitterException;

/**
 * Provides the necessary interfaces for the twitter connector
 * 
 * @author pl
 * 
 */
public interface TwitterConnector {

	/** Used to post a message on twitter */
	public List<String> getMessages(String score) throws TwitterException;
}
