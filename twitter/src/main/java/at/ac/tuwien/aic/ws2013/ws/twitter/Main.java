package at.ac.tuwien.aic.ws2013.ws.twitter;

import java.util.Scanner;

import javax.xml.ws.Endpoint;

public class Main {

	public static void main(String[] args) {
		TwitterWS ws = new TwitterWSImpl();
		Endpoint endpoint = Endpoint.publish(
				"http://127.0.0.1:8082/TwitterWebService", ws);

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter exit to stop webservice");

		while (scanner.hasNextLine()) {
			if (scanner.nextLine().equals("exit")) {
				endpoint.stop();
				break;
			}
		}

		scanner.close();
	}
}
