package at.ac.tuwien.aic.ws2013.ws.twitter;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import twitter4j.TwitterException;

@WebService(name = "TwitterWS", targetNamespace = "http://twitter.ws.ws2013.aic.tuwien.ac.at/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
public class TwitterWSImpl implements TwitterWS {

	@Override
	@WebMethod
	@WebResult(name = "StringListResponse", targetNamespace = "aicTypes", partName = "parameters")
	public StringListResponse getTwitterMessages(
			@WebParam(name = "StringContainer", targetNamespace = "aicTypes", partName = "parameters") StringContainer parameters) {
		TwitterConnector connector = new TwitterConnectorImpl();
		StringListResponse response = new StringListResponse();
		try {
			List<String> messages = connector.getMessages(parameters.name);

			response.getValue().addAll(messages);
			return response;
		} catch (TwitterException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}
}