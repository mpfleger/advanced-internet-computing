package at.ac.tuwien.aic.ws2013.ws.twitter;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

@WebService(name = "TwitterWS", targetNamespace = "http://twitter.ws.ws2013.aic.tuwien.ac.at/")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
public class FakeTwitterService implements TwitterWS {

	private int counter = 1;
	
	@Override
	@WebMethod
	@WebResult(name = "StringListResponse", targetNamespace = "aicTypes", partName = "parameters")
	public StringListResponse getTwitterMessages(
			@WebParam(name = "StringContainer", targetNamespace = "aicTypes", partName = "parameters") StringContainer parameters) {
		System.out.println(parameters.getName());
		StringListResponse rp = new StringListResponse();
		rp.getValue().add("Twitter: " + parameters.getName() + counter++);
		return rp;
	}
}
