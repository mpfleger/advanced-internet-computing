package at.ac.tuwien.aic.ws2013.ws.twitter;

import java.util.ArrayList;
import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import com.google.common.base.CharMatcher;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author AJusits
 */
public class TwitterConnectorImpl implements TwitterConnector {

	private String consumerKey = "bRWM90o1U6r8zOn3qVds6g";
	private String consumerSecret = "MgPYlzS5bW6jLey9ePA0bVPtUw1qoJigdrudGk1N68";
	private String accessToken = "2199953330-AjyT6EgrTos058UfB50zKAg3i4bNyGl4GcCMyH7";
	private String accessTokenSecret = "RSZXO4o57aZBKwKxmuMtyQxcXLRIUiy9PW6fOmnp6mCeF";
	private Twitter twitter;

	public TwitterConnectorImpl() {
		this.twitter = new TwitterFactory().getInstance();
		this.twitter.setOAuthConsumer(consumerKey, consumerSecret);
		this.twitter.setOAuthAccessToken(new AccessToken(accessToken,
				accessTokenSecret));

	}

	@Override
	public List<String> getMessages(String searchText) throws TwitterException {
		System.out.println(String.format("Gettings tweets for %s", searchText));
		List<String> messages = new ArrayList<String>();
		Query query = new Query(searchText);

		QueryResult result = twitter.search(query);
		for (Status status : result.getTweets()) {
			String s = status.getText();
			s = s.substring(0, Math.min(s.length(), 54));
			if (CharMatcher.ASCII.matchesAllOf(s)) {
				System.out.println(s);
				messages.add(s);
			} else {
				System.out.println("Filtered: " + s);
			}
		}
		return messages;
	}

}
